[Real time preview](https://tender-shockley-74a8d0.netlify.app/)

I have done as much as I manage to do in around 4 hours...

# Sum Up

Application is made in the newest version of Angular, I decided to not use any CSS or UI frameworks as it was requested. To manage state application and store data I decided to you NGXS with library of entity-state lib.

I haven't done filters in transaction history so I do not cover every User Story. I thinking that for UI-element of choosing method of sort, I could create custom component with ControlValueAccessor and create fromGroup with filters.

In transaction form (transfer) I do not show the error of amount control, also I have planned to create custom validator to cover User Story about -500.00 limit.

And the rest od TODO list:
1. Configure and run test,
2. Implement translation system,
3. Add error handling,
3. Polish everything,



# Jcommerce

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
