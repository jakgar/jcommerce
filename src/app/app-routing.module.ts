import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransactionContainerComponent } from './components/transaction-container/transaction-container.component';

const routes: Routes = [
  { path: 'home', component: TransactionContainerComponent },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
