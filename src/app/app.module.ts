import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TransactionContainerComponent } from './components/transaction-container/transaction-container.component';
import { HeaderComponent } from './components/header/header.component';
import { TransferFormComponent } from './components/transfer-form/transfer-form.component';
import { ResentTransactionsComponent } from './components/resent-transactions/resent-transactions.component';
import { TransactionComponent } from './components/transaction/transaction.component';
import { CardHeaderComponent } from './card-header/card-header.component';
import { environment } from 'src/environments/environment';
import { TransactionState } from './store/transaction/transaction.state';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TransactionImgPipe } from './shared/pipes/transaction-img.pipe';
import { TransactionFiltersComponent } from './components/transaction-filters/transaction-filters.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TransferFormComponent,
    ResentTransactionsComponent,
    TransactionComponent,
    TransactionContainerComponent,
    CardHeaderComponent,
    TransactionImgPipe,
    TransactionFiltersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxsModule.forRoot([TransactionState], {
      developmentMode: !environment.production
    }),
    NgxsReduxDevtoolsPluginModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
