import { Injectable } from '@angular/core';
import { Add, CreateOrReplace, defaultEntityState, EntityState, EntityStateModel, IdStrategy } from '@ngxs-labs/entity-state';
import { NgxsOnInit, State, StateContext } from '@ngxs/store';
import { TransactionModel } from 'src/app/models/transaction.model';
import { TransactionService } from 'src/app/services/transaction.service';

@State<EntityStateModel<TransactionModel>>({
  name: 'transaction',
  defaults: defaultEntityState()
})
@Injectable()
export class TransactionState extends EntityState<TransactionModel> implements NgxsOnInit {
  constructor(private transactionSer: TransactionService) {
    super(TransactionState, 'id', IdStrategy.UUIDGenerator);
  }

  public ngxsOnInit({ dispatch }: StateContext<TransactionModel>): void {
    this.transactionSer.getTransaction().subscribe((transactions) => dispatch(new Add(TransactionState, transactions)));
  }
}
