import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResentTransactionsComponent } from './resent-transactions.component';

describe('ResentTransactionsComponent', () => {
  let component: ResentTransactionsComponent;
  let fixture: ComponentFixture<ResentTransactionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResentTransactionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResentTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
