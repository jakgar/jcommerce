import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { TransactionModel } from 'src/app/models/transaction.model';
import { TransactionState } from 'src/app/store/transaction/transaction.state';

@Component({
  selector: 'app-resent-transactions',
  templateUrl: './resent-transactions.component.html',
  styleUrls: ['./resent-transactions.component.scss']
})
export class ResentTransactionsComponent implements OnInit {
  @Select(TransactionState.entities) public rawTransactions$!: Observable<TransactionModel[]>;
  public transactions$!: Observable<TransactionModel[]>;

  constructor() { }

  ngOnInit(): void {
    this.transactions$ = this.rawTransactions$.pipe(
      map((transactions: TransactionModel[]) => transactions.sort((a, b) => b.dates.valueDate - a.dates.valueDate)),
    );
  }
}
