import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Add } from '@ngxs-labs/entity-state';
import { Store } from '@ngxs/store';
import { take } from 'rxjs/operators';
import { TransactionModel } from 'src/app/models/transaction.model';
import { TransactionState } from 'src/app/store/transaction/transaction.state';

export interface FormValue {
  fromAccount: string;
  toAccount: string;
  amount: number;
}

@Component({
  selector: 'app-transfer-form',
  templateUrl: './transfer-form.component.html',
  styleUrls: ['./transfer-form.component.scss']
})
export class TransferFormComponent implements OnInit {
  private FROM_ACCOUNT_DEFAULT = 'Free Checking (4692)';
  private TO_ACCOUNT_DEFAULT = 'Georgia Power Electric Company';
  public formGroup!: FormGroup;
  public showDetails = false;
  public formValue!: FormValue;


  constructor(private fb: FormBuilder, private store: Store) { }

  public ngOnInit(): void {
    this.formGroup = this.createForm();

    this.formGroup.get('fromAccount')?.disable();
    this.formGroup.get('toAccount')?.disable();
  }

  public submitForm(): void {
    if (this.formGroup.invalid) {
      return;
    }

    this.showDetails = true;
    this.formValue = { ...this.formGroup.getRawValue() };

  }

  public transfer(): void {
    console.log('transfer');
    const newTransaction: TransactionModel = {
      categoryCode: '#12a580',
      dates: {
        valueDate: Date.now()
      },
      merchant: {
        name: this.formValue.toAccount,
        accountNumber: ''
      },
      transaction: {
        amountCurrency: {
          amount: this.formValue.amount,
          currencyCode: 'EUR'
        },
        type: 'Online Transfer',
      }
    };

    this.store.dispatch(new Add(TransactionState, newTransaction)).pipe(
      take(1)).subscribe(() => {
        this.formGroup.get('amount')?.reset();
        this.showDetails = false;
      });
  }

  private createForm(): FormGroup {
    return this.fb.group({
      fromAccount: this.FROM_ACCOUNT_DEFAULT,
      toAccount: this.TO_ACCOUNT_DEFAULT,
      amount: ['', [Validators.required, Validators.pattern(/^-?\d+\.?\d*$/)]]
    });
  }
}
