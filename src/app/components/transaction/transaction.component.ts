import { Component, Input, OnInit } from '@angular/core';
import { TransactionModel } from 'src/app/models/transaction.model';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {
  @Input() public transaction!: TransactionModel;

  constructor() { }

  ngOnInit(): void {
  }

}
